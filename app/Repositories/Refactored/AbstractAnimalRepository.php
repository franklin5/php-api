<?php

namespace App\Repositories\Refactored;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractAnimalRepository
{
    protected Model $model;

    public function getAllWithPagination(array $options = [], array $relations = []): ?LengthAwarePaginator
    {
        $pageSize = $options['pageSize'] ?? config('pagination.default_size');

        return $this->model::with($relations)->paginate($pageSize);
    }

    public function getById(int $id, array $relations = []): ?Model
    {
        return $this->model::with($relations)->find($id);
    }

    public function store(array $options): ?Model
    {
        return $this->setFieldsAndPersist($this->model, $options);
    }

    protected function setFieldsAndPersist(Model $model, array $options): ?Model
    {
        $model->fact = $options['fact'];

        if ($model->save()) {
            return $model;
        }

        return null;
    }
}
