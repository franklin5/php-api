<?php
declare(strict_types=1);

namespace App\Repositories\Refactored;

use App\Contract\AnimalRepositoryInterface;
use App\Models\CatFact;
use Illuminate\Database\Eloquent\Model;

class CatFactRepository extends AbstractAnimalRepository implements AnimalRepositoryInterface
{
    public function __construct(CatFact $dogFact){
        $this->model = $dogFact;
    }

    public function update(CatFact $catFact, array $options): ?Model
    {
        return $this->setFieldsAndPersist($catFact, $options);
    }

    public function destroy(CatFact $catFact): bool
    {
        return $catFact->delete();
    }
}
