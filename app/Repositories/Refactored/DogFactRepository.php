<?php
declare(strict_types=1);

namespace App\Repositories\Refactored;

use App\Contract\AnimalRepositoryInterface;
use App\Models\DogFact;
use Illuminate\Database\Eloquent\Model;

class DogFactRepository extends AbstractAnimalRepository implements AnimalRepositoryInterface
{
    public function __construct(DogFact $dogFact){
        $this->model = $dogFact;
    }

    public function update(DogFact $dogFact, array $options): ?Model
    {
        return $this->setFieldsAndPersist($dogFact, $options);
    }

    public function destroy(DogFact $dogFact): bool
    {
        return $dogFact->delete();
    }
}
