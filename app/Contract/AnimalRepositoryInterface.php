<?php

namespace App\Contract;

use App\Models\CatFact;
use App\Models\DogFact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface AnimalRepositoryInterface
{
    public function getAllWithPagination(array $options = [], array $relations = []): ?LengthAwarePaginator;

    public function getById(int $id, array $relations = []): ?Model;

    public function store(array $options): ?Model;
}
